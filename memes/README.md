# Memes

memes may be used to lighten up the mood while presenting. The used memes are from a private collection and we weren't able to directly attribute them.

If you find your work here we'll happily attribute/ acknowledge or remove it.

> Please [Contact us](mailto:chaostheorie@pm.me) for related inquiries, we will reply in under 24 hours.

The content will be used for educational purposes and all content produced by us and published on gitlab will be be under GPL v3.0. There will and hasn't been any commercial distribution of the content and assets.
