# Presentations

Collection of all used presentations and their materials.

## Licensing

See our [License](https://gitlab.com/seminar-informatik-20/seminar-informatik-20.gitlab.io/-/blob/master/LICENSE).

The content will be used for educational purposes and all content produced by us and published on gitlab will be be under GPL v3.0. There will and hasn't been any commercial distribution of the content and used assets.

## Attributions

- [Python Logo SVG](www.python.org) by [Python Software Foundation](https://www.python.org/psf/trademarks/) under
- [Flask Logo SVG](http://flask.pocoo.org/static/logo/flask.svg) by Armin Ronacher (modified)
- [Fontawesome Solid icons](https://fontawesome.com/) under [Creative Commons Attribution 4.0 International license @ Fonticons, Inc., a Delaware corporation](https://fontawesome.com/license)
- [Cat skeleton](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fres.cloudinary.com%2Fdk-find-out%2Fimage%2Fupload%2Fq_80%2Cw_1920%2Cf_auto%2FDCTM_Penguin_UK_DK_AL510987_ncedxp.jpg&f=1&nofb=1)
- [Cat](https://mainecoon-abc.de/gesundheit/)
